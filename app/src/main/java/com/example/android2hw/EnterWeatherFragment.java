package com.example.android2hw;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


public class EnterWeatherFragment extends Fragment {


    public static final String CITY_NAME = "CITY_NAME";
    private TextView finishText;
    private LinearLayout addCity;
    private EditText setCity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_enter, container, false);

        setCity = v.findViewById(R.id.et_set_city);
        setCity.setVisibility(View.INVISIBLE);

        registerForContextMenu(v);

        finishText = v.findViewById(R.id.push_finish);
        finishText.setOnClickListener((view) -> {
            if (getActivity() != null) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                if (setCity.getVisibility() == View.VISIBLE && setCity.getText().toString().length() > 3) {
                    intent.putExtra(CITY_NAME, setCity.getText().toString());
                }
                startActivity(intent);
            }
        });

        addCity = v.findViewById(R.id.ll_add_city);
        addCity.setOnClickListener((view) -> setCity.setVisibility(View.VISIBLE));

        return v;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if(getActivity() != null) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.context_menu_change, menu);
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(getView() != null) {
            switch (item.getItemId()) {
                case R.id.menu_refresh:
                    Snackbar.make(getView(), "Refreshed", Snackbar.LENGTH_SHORT).show();
                    return true;
                default:
                    return super.onContextItemSelected(item);
            }
        }
        return super.onContextItemSelected(item);
    }
}
