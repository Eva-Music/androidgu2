package com.example.android2hw;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String ADD_CITY = "ADD_CITY";
    public static final String SETTINGS = "SETTINGS";
    private TextView toolbarTitle;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbarTitle = findViewById(R.id.toolbar_title);

        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toogle = new ActionBarDrawerToggle(this,
                drawerLayout, toolbar, R.string.refresh, R.string.finish);
        drawerLayout.addDrawerListener(toogle);
        toogle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener((item) -> {
            int id = item.getItemId();

            if (id == R.id.nav_contact) {
                Intent intent = new Intent(this, SecondActivity.class);
                intent.putExtra(SETTINGS, "settings");
                startActivity(intent);
            }
            if (id == R.id.nav_share) {
                Snackbar.make(item.getActionView(), "Share", Snackbar.LENGTH_SHORT).show();

            }

            drawerLayout.closeDrawer(GravityCompat.START);
            return true;

        });


        if (getIntent() == null) {
            toolbarTitle.setText("");
        } else {
            String cityName = getIntent().getStringExtra(EnterWeatherFragment.CITY_NAME);
            toolbarTitle.setText(cityName);
        }

        WeatherFragment weatherFragment = new WeatherFragment();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container_for_fragments, weatherFragment)
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container_for_fragments, weatherFragment)
                    .commit();
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.add_menu_icon) {

            Intent intent = new Intent(this, SecondActivity.class);
            intent.putExtra(ADD_CITY, "add");
            startActivity(intent);

        }
        return super.onOptionsItemSelected(item);
    }
}
