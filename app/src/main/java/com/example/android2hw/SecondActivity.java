package com.example.android2hw;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        if ((getIntent() != null) && (getIntent().getStringExtra(MainActivity.ADD_CITY) != null)) {
            String code = getIntent().getStringExtra(MainActivity.ADD_CITY);
            if (code.equals("add")) {
                EnterWeatherFragment enterWeatherFragment = new EnterWeatherFragment();

                if (savedInstanceState == null) {
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.container_for_menu_pages, enterWeatherFragment).commit();
                } else {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container_for_menu_pages, enterWeatherFragment)
                            .addToBackStack(null).commit();
                }

            }
        }
        if ((getIntent() != null) && (getIntent().getStringExtra(MainActivity.SETTINGS) != null)) {
            String code = getIntent().getStringExtra(MainActivity.SETTINGS);
            if (code.equals("settings")) {
                ContactFragment contactFragment = new ContactFragment();
                if (savedInstanceState == null) {
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.container_for_menu_pages, contactFragment)
                            .commit();
                } else {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container_for_menu_pages, contactFragment)
                            .addToBackStack(null)
                            .commit();

                }

            }
        }

    }
}
